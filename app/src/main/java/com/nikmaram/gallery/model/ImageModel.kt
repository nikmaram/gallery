package com.nikmaram.gallery.model

import android.net.Uri

data class ImageModel(val imageURL: Uri)