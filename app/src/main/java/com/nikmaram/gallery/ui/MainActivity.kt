package com.nikmaram.gallery.ui

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RestrictTo
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.nikmaram.gallery.R
import com.nikmaram.gallery.databinding.ActivityMainBinding
import com.nikmaram.gallery.model.ImageModel
import com.nikmaram.gallery.util.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.withContext


class MainActivity : AppCompatActivity() {
    lateinit var resultLauncherCamera: ActivityResultLauncher<Intent>
    lateinit var resultLauncherGallery: ActivityResultLauncher<Intent>
    lateinit var binding: ActivityMainBinding
    lateinit var controller:PhotoController
    var images = mutableListOf<ImageModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.btnGallery.setOnClickListener {
            this.showDialogGeneral(cameraCallback = {
                pickImageFromCamera()
            }, galleryCallback = {
                pickImageFromGallery()
            })
        }
        controller = PhotoController() {
                index -> images.removeAt(index)
                controller.images = images
                if (images.isEmpty())
                    binding.text.visibility = View.VISIBLE
        }
        binding.recyclerView.setController(controller)
    }

    private fun pickImageFromCamera() {
        if (this.checkPermissionCamera() || Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                startActivityForCamera()
        } else {
            this.requestPermissionCamera()
        }
    }

    private fun startActivityForCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        resultLauncherCamera.launch(cameraIntent)
    }

    private fun addImage(uri: Uri) {
         images.add(ImageModel(uri))
        controller.images = images
        binding.text.visibility = View.GONE
    }

    private fun pickImageFromGallery() {
       val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        resultLauncherGallery.launch(galleryIntent)
    }

    override fun onStart() {
        super.onStart()
        resultLauncherCamera =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == RESULT_OK) {
                    result.data?.extras?.let{
                    val imageBitmap = it.get("data") as Bitmap
                    compressAndSetImage(imageBitmap, this) { uri ->
                        addImage(uri)}
                }
            }
            }
         resultLauncherGallery = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                result.data?.data?.let {
                    compressAndSetImage(it, this) { uri ->
                        addImage(uri)
                    }
                }
            }
        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivityForCamera()
            }
        }
    }

}