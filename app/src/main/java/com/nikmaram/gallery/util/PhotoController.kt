package com.nikmaram.gallery.util

import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.Typed2EpoxyController
import com.nikmaram.gallery.image
import com.nikmaram.gallery.model.ImageModel

class PhotoController(private val callBack: (index:Int) -> Unit) : EpoxyController() {
    var images: List<ImageModel> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        images.forEachIndexed { index, imageModel ->
            image {
                id("item$index")
                onClick { _ -> callBack(index) }
                image(imageModel.imageURL)
            }
        }
    }

}