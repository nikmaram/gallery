package com.nikmaram.gallery.util

import android.app.Activity
import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import android.webkit.MimeTypeMap
import id.zelory.compressor.Compressor
import id.zelory.compressor.constraint.format
import id.zelory.compressor.constraint.quality
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.*

fun compressAndSetImage(imageBitmap: Bitmap,context:Activity,result:(Uri)->Unit){
   try {
       val bytes = ByteArrayOutputStream()
       imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
       val path: String = MediaStore.Images.Media.insertImage(
           context.contentResolver,
           imageBitmap,
           "image",
           null
       )
       val uri = Uri.parse(path)
       result(uri)
   }catch (ex:Exception){
       Log.e("compressAndSetImage", ex.toString() )
   }

}

fun compressAndSetImage(uri: Uri,context:Activity,result:(Uri)->Unit){
    try {
        val scope = CoroutineScope(Dispatchers.IO)
        val fileUri = getFilePathFromUri(uri, context)
        scope.launch {
            val compressedImageFile = Compressor.compress(context, File(fileUri?.path ?:"")){
                quality(90) // combine with compressor constraint
                format(Bitmap.CompressFormat.JPEG)
            }
            val resultUri = Uri.fromFile(compressedImageFile)
            withContext(Dispatchers.Main){
                resultUri?.let {
                    result(it)
                }
            }

        }
    }catch (ex:Exception){
        Log.e("compressAndSetImage", ex.toString() )
    }

}
fun getFilePathFromUri(uri: Uri?, context: Context?): Uri? {
    val fileName: String? = getFileName(uri, context)
    val file = File(context?.externalCacheDir, fileName)
    file.createNewFile()
    FileOutputStream(file).use { outputStream ->
        if (uri != null) {
            context?.contentResolver?.openInputStream(uri).use { inputStream ->
                copyFile(inputStream, outputStream)
                outputStream.flush()
            }
        }
    }
    return Uri.fromFile(file)
}

private fun copyFile(`in`: InputStream?, out: OutputStream) {
    val buffer = ByteArray(1024)
    var read: Int? = null
    while (`in`?.read(buffer).also { read = it!! } != -1) {
        read?.let { out.write(buffer, 0, it) }
    }
}//copyFile ends

fun getFileName(uri: Uri?, context: Context?): String? {
    var fileName: String? = getFileNameFromCursor(uri, context)
    if (fileName == null) {
        val fileExtension: String? = getFileExtension(uri, context)
        fileName = "temp_file" + if (fileExtension != null) ".$fileExtension" else ""
    } else if (!fileName.contains(".")) {
        val fileExtension: String? = getFileExtension(uri, context)
        fileName = "$fileName.$fileExtension"
    }
    return fileName
}

fun getFileExtension(uri: Uri?, context: Context?): String? {
    val fileType: String? = uri?.let { context?.contentResolver?.getType(it) }
    return MimeTypeMap.getSingleton().getExtensionFromMimeType(fileType)
}

fun getFileNameFromCursor(uri: Uri?, context: Context?): String? {
    val fileCursor: Cursor? = uri?.let {
        context?.contentResolver
        ?.query(it, arrayOf<String>(OpenableColumns.DISPLAY_NAME), null, null, null)
    }
    var fileName: String? = null
    if (fileCursor != null && fileCursor.moveToFirst()) {
        val cIndex: Int = fileCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        if (cIndex != -1) {
            fileName = fileCursor.getString(cIndex)
        }
    }
    return fileName
}