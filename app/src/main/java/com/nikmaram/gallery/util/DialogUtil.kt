package com.nikmaram.gallery.util
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.nikmaram.gallery.R
import com.nikmaram.gallery.databinding.DialogGeneralBinding


fun Context.showDialogGeneral(galleryCallback: () -> Unit = {}, cameraCallback: () -> Unit = {}): Dialog {
    val dialog = Dialog(this, R.style.Theme_Dialog)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    val binding: DialogGeneralBinding = DataBindingUtil.inflate(
        LayoutInflater.from(this),
        R.layout.dialog_general,
        null,
        false
    )
    dialog.setContentView(binding.root)
    dialog.setCancelable(true)
    val lp = WindowManager.LayoutParams()
    lp.copyFrom(dialog.window!!.attributes)
    binding.btnCamera.setOnClickListener {
        cameraCallback()
        dialog.dismiss()
    }

    binding.btnGallery.setOnClickListener {
        galleryCallback()
        dialog.dismiss()
    }

    dialog.show()
    dialog.window!!.attributes = lp
    return dialog
}