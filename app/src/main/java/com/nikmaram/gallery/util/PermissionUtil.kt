package com.nikmaram.gallery.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

const val PERMISSION_REQUEST_CODE = 200

fun Context.checkPermissionCamera() = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED

fun Activity.requestPermissionCamera() {
    ActivityCompat.requestPermissions(
        this, arrayOf(Manifest.permission.CAMERA),
        PERMISSION_REQUEST_CODE
    )
}
