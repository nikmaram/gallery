package com.nikmaram.gallery.util;

import com.airbnb.epoxy.EpoxyDataBindingPattern;
import com.nikmaram.gallery.R;

@EpoxyDataBindingPattern(rClass = R.class, layoutPrefix = "item")
public interface EpoxyDataBindingConfig { }
